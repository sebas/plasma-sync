/*************************************************************************************
 *  Copyright (C) 2012 by Alejandro Fiestas Olivares <afiestas@kde.org>              *
 *  Copyright     2012 by Sebastian Kügler <sebas@kde.org>                           *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#include "../src/daemon.h"

#include <QtTest/QtTest>
#include <QtCore/QObject>


Q_LOGGING_CATEGORY(PLASMASYNC, "plasmasync");

using namespace PlasmaSync;

class testDaemon : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase();

    void localPath();

    void upload();
    void update();

    void cleanupTestCase();

private:
    QByteArray md5sum(const QString &file) const;
    Daemon *m_daemon;
    QString m_testData;
};

void testDaemon::initTestCase()
{
    m_testData = QFINDTESTDATA("data/");
    QString remote = m_testData + "remote/";
    setenv("PLASMASYNC_BACKEND", "kio", 1);
    setenv("PLASMASYNC_KIO_REMOTE", remote.toLocal8Bit(), 1);


    m_daemon = Daemon::instance();
    QVERIFY(m_daemon != 0);
}

void testDaemon::localPath()
{
    qCDebug(PLASMASYNC) << "Test data found in: " << m_testData;
    QVERIFY(QDir(m_testData + QStringLiteral("local")).exists());
}

void testDaemon::upload()
{
    // The percent encoding is internal to the KIO backend, so this test will fail
    // for other backends.
    QString file = m_testData + "/local/localrc";
    QString remotefile = m_testData + "remote/" + QUrl::toPercentEncoding(file);
    QVERIFY(QFile(file).exists());
    QByteArray sourcehash = md5sum(file);
    m_daemon->upload(file);
    QSignalSpy spy(m_daemon, SIGNAL(uploaded(const QString&)));
    bool uploadsucceeded = spy.wait(100);
    QByteArray remotehash = md5sum(remotefile);
    QCOMPARE(sourcehash, remotehash);
    QVERIFY(uploadsucceeded);
    QFile::remove(remotefile);

}

void testDaemon::update()
{
    // The percent encoding is internal to the KIO backend, so this test will fail
    // for other backends.
    QString file = m_testData + "/local/remoterc";
    QString remotefile = m_testData + "remote/" + QUrl::toPercentEncoding(file);
    QFile::copy(m_testData + "/remote/remoterc", remotefile);
    QVERIFY(QFile(remotefile).exists());
    m_daemon->update(file);
    QSignalSpy spy(m_daemon, SIGNAL(updated(const QString&)));
    bool downloadsucceeded = spy.wait(100);
    QByteArray remotehash = md5sum(remotefile);
    QByteArray sourcehash = md5sum(file);
    QCOMPARE(sourcehash, remotehash);
    QVERIFY(downloadsucceeded);
    QFile::remove(file);
    QFile::remove(remotefile);
}

void testDaemon::cleanupTestCase()
{
    delete m_daemon;
    //qApp->exit(0);
}

QByteArray testDaemon::md5sum(const QString& file) const
{
    QFile f(file);
    f.open(QIODevice::ReadOnly);
    QByteArray contents = f.readAll();
    return QCryptographicHash::hash(contents, QCryptographicHash::Md5);
}


QTEST_MAIN(testDaemon)

#include "testdaemon.moc"
