project(plasma-sync)

cmake_minimum_required(VERSION 2.8.12)

find_package(ECM 0.0.11 REQUIRED NO_MODULE)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ECM_MODULE_PATH} ${ECM_KDE_MODULE_DIR})
include(KDEInstallDirs)
include(KDECompilerSettings)
include(KDECMakeSettings)
include(ECMSetupVersion)
include(ECMPackageConfigHelpers)
include(ECMMarkAsTest)
include(ECMGenerateHeaders)
include(FeatureSummary)
include(CheckCXXCompilerFlag)

set(REQUIRED_QT_VERSION 5.2.0)
find_package(Qt5 ${REQUIRED_QT_VERSION} CONFIG REQUIRED Core DBus Gui Test)

set(KF5_VERSION "5.0.91") #When we are happy with the api, we can sync with frameworks

# TODO: make kio and kconfig optional
find_package(KF5 ${KF5_VERSION} REQUIRED COMPONENTS KIO Config)

ecm_setup_version(${KF5_VERSION} VARIABLE_PREFIX PLASMASYNC
                        VERSION_HEADER "${CMAKE_CURRENT_BINARY_DIR}/plasmasync_version.h"
                        PACKAGE_VERSION_FILE "${CMAKE_CURRENT_BINARY_DIR}/KF5PlasmaSyncConfigVersion.cmake"
                        SOVERSION 5)

check_cxx_compiler_flag(-fvisibility=hidden _HAVE_VISIBILITY)
if (_HAVE_VISIBILITY AND NOT WIN32)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fvisibility=hidden")

  check_cxx_compiler_flag(-fvisibility-inlines-hidden _HAVE_VISIBILITY_INLINES)
  if (_HAVE_VISIBILITY_INLINES)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fvisibility-inlines-hidden")
  endif (_HAVE_VISIBILITY_INLINES)
endif (_HAVE_VISIBILITY AND NOT WIN32)

# include_directories(${CMAKE_SOURCE_DIR} ${CMAKE_BINARY_DIR} ${CMAKE_CURRENT_BINARY_DIR})

include_directories(${CMAKE_BINARY_DIR}/src)

add_subdirectory(src)
add_subdirectory(backends)
add_subdirectory(autotests)
add_subdirectory(tests)

set(CMAKECONFIG_INSTALL_DIR "${CMAKECONFIG_INSTALL_PREFIX}/KF5PlasmaSync")
ecm_configure_package_config_file("${CMAKE_CURRENT_SOURCE_DIR}/KF5PlasmaSyncConfig.cmake.in"
                              "${CMAKE_CURRENT_BINARY_DIR}/KF5PlasmaSyncConfig.cmake"
                              INSTALL_DESTINATION ${CMAKECONFIG_INSTALL_DIR}
)

install(FILES
  "${CMAKE_CURRENT_BINARY_DIR}/KF5PlasmaSyncConfig.cmake"
  "${CMAKE_CURRENT_BINARY_DIR}/KF5PlasmaSyncConfigVersion.cmake"
  DESTINATION "${CMAKECONFIG_INSTALL_DIR}"
  COMPONENT Devel
)

install(EXPORT
  KF5PlasmaSyncTargets
  DESTINATION "${CMAKECONFIG_INSTALL_DIR}"
  FILE KF5PlasmaSyncTargets.cmake
  NAMESPACE KF5::
  COMPONENT Devel
)

install(FILES
  "${CMAKE_CURRENT_BINARY_DIR}/plasmasync_version.h"
  DESTINATION "${KF5_INCLUDE_INSTALL_DIR}"
  COMPONENT Devel
)

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)

############################################ apidox ############################################

option(LIBPLASMASYNC_BUILD_API_DOCS "Build plasma-sync API documentation")

if(LIBPLASMASYNC_BUILD_API_DOCS)
  find_package(Doxygen)

  if(DOXYGEN_EXECUTABLE)
    configure_file(${plasma-sync_SOURCE_DIR}/.Doxyfile.cmake ${plasma-sync_BINARY_DIR}/Doxyfile)

    if(EXISTS ${QT_DOC_DIR}/html)
      set(QTDOCS "${QT_DOC_DIR}/html")
    else(EXISTS ${QT_DOC_DIR}/html)
      set(QTDOCS "http://doc.qt.nokia.com/latest/")
    endif(EXISTS ${QT_DOC_DIR}/html)

    add_custom_target(
      apidox ALL
      COMMAND ${DOXYGEN_EXECUTABLE} Doxyfile
      COMMAND doc/html/installdox -l qt4.tag@${QTDOCS} doc/html/*.html)
  endif(DOXYGEN_EXECUTABLE)
endif(LIBPLASMASYNC_BUILD_API_DOCS)
