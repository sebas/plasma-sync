/*************************************************************************************
 *  Copyright 2014 Sebastian Kügler <sebas@kde.org>                                  *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#include "plasmasynctest.h"
#include "ui_plasmasynctest.h"

#include <QPushButton>
#include <QLabel>
#include <QComboBox>
#include <QTextEdit>

#include <KSharedConfig>
#include <KConfigGroup>
#include <KDirWatch>

#include <QDebug>


plasmasynctest::plasmasynctest(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::plasmasynctest)
{
    ui->setupUi(this);

    connect(ui->save, &QPushButton::clicked, this, &plasmasynctest::save);
    connect(ui->reload, &QPushButton::clicked, this, &plasmasynctest::load);

    const QString configFile = QStandardPaths::writableLocation(QStandardPaths::GenericConfigLocation) + QStringLiteral("/plasmasync-testclientrc");
    KDirWatch::self()->addFile(configFile);

    // Catch both, direct changes to the config file ...
    connect(KDirWatch::self(), &KDirWatch::dirty, this, &plasmasynctest::load);
    // ... but also remove/recreate cycles, like KConfig does it
    connect(KDirWatch::self(), &KDirWatch::created, this, &plasmasynctest::load);
    // Trigger configuration read
    load();
}

plasmasynctest::~plasmasynctest()
{
    delete ui;
}

void plasmasynctest::load()
{
    KSharedConfig::Ptr config = KSharedConfig::openConfig("plasmasync-testclientrc");
    KConfigGroup cfg(config, "Settings");
    cfg.config()->reparseConfiguration();

    comboIndex = cfg.readEntry("comboIndex", 1);
    text = cfg.readEntry("text", "I hung my head.");
    status = cfg.readEntry("status", "Read from config.");
    checkbox = cfg.readEntry("checkbox", true);

    qDebug() << "Settings: index " << cfg.readEntry("comboIndex", 1);
    qDebug() << "Settings: text " << cfg.readEntry("text", "I hung my head.");
    qDebug() << "Settings: index " << cfg.readEntry("comboIndex", 1);
    qDebug() << "Settings: text " << cfg.readEntry("text", "I hung my head.");

    updateUi();
}

void plasmasynctest::updateUi()
{
    ui->comboBox->setCurrentIndex(comboIndex);
    ui->checkBox->setChecked(checkbox);
    ui->textEdit->setText(text);
    ui->label->setText(status);
}


void plasmasynctest::save()
{
    qDebug() << "Save";
    KSharedConfig::Ptr config = KSharedConfig::openConfig("plasmasync-testclientrc");
    KConfigGroup cfg(config, "Settings");

    cfg.writeEntry("comboIndex", ui->comboBox->currentIndex());
    cfg.writeEntry("checkbox", ui->checkBox->isChecked());
    cfg.writeEntry("text", ui->textEdit->toPlainText());
    cfg.writeEntry("status", ui->label->text());

    cfg.config()->sync();
}

