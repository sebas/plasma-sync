/*************************************************************************************
 *  Copyright 2014 Sebastian Kügler <sebas@kde.org>                                  *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#ifndef PLASMASYNCTEST_H
#define PLASMASYNCTEST_H

#include <QMainWindow>

namespace Ui {
class plasmasynctest;
}

namespace PlasmaSync {
    class Daemon;
}

class plasmasynctest : public QMainWindow
{
    Q_OBJECT

public:
    explicit plasmasynctest(QWidget *parent = 0);
    ~plasmasynctest();

private:
    void load();
    void save();
    void updateUi();
    Ui::plasmasynctest *ui;
    PlasmaSync::Daemon *m_daemon;
    QString m_testData;

    int comboIndex;
    QString text;
    bool checkbox;
    QString status;
};

#endif // PLASMASYNCTEST_H
