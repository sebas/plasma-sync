/*************************************************************************************
 *  Copyright 2014 Sebastian Kügler <sebas@kde.org>                                  *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#ifndef PLASMASYNCTEST_H
#define PLASMASYNCTEST_H

#include <QMainWindow>

namespace Ui {
class plasmasynctest;
}

namespace PlasmaSync {
    class Daemon;
}

class TestDaemon : public QMainWindow
{
    Q_OBJECT

public:
    explicit TestDaemon(QWidget *parent = 0);
    ~TestDaemon();

    void startDaemon();

private:
    void upload();
    void update();
    void cleanup();
    void runall();

    void updateConfig();
    void uploadConfig();

    QString sanitize(const QString &t) const;
    Ui::plasmasynctest *ui;
    PlasmaSync::Daemon *m_daemon;
    QString m_testData;
    QString m_testConfig;
};

#endif // PLASMASYNCTEST_H
