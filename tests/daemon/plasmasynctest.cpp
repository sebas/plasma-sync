/*************************************************************************************
 *  Copyright 2014 Sebastian Kügler <sebas@kde.org>                                  *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#include "plasmasynctest.h"
#include "ui_plasmasynctest.h"

#include <QTest>

#include <QPushButton>
#include <QLabel>

#include <QSignalSpy>
#include <QStandardPaths>
#include <QCryptographicHash>

#include <PlasmaSync/Daemon>

using namespace PlasmaSync;

QByteArray md5sum(const QString& file)
{
    QFile f(file);
    f.open(QIODevice::ReadOnly);
    QByteArray contents = f.readAll();
    return QCryptographicHash::hash(contents, QCryptographicHash::Md5);
}


TestDaemon::TestDaemon(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::plasmasynctest)
{
    ui->setupUi(this);
    m_testData = QFINDTESTDATA("../../autotests/data/");

    m_testConfig = QStandardPaths::writableLocation(QStandardPaths::GenericConfigLocation) + "/plasmasync-testclientrc";

    QString remote = m_testData + "remote/";
    const QString envremote = qgetenv("PLASMASYNC_KIO_REMOTE").constData();
    if (envremote.isEmpty()) {
        setenv("PLASMASYNC_KIO_REMOTE", remote.toLocal8Bit(), 1);
    }

    connect(ui->upload, &QPushButton::clicked, this, &TestDaemon::upload);
    connect(ui->update, &QPushButton::clicked, this, &TestDaemon::update);
    connect(ui->cleanup, &QPushButton::clicked, this, &TestDaemon::cleanup);
    connect(ui->runall, &QPushButton::clicked, this, &TestDaemon::runall);

    connect(ui->updateConfig, &QPushButton::clicked, this, &TestDaemon::updateConfig);
    connect(ui->uploadConfig, &QPushButton::clicked, this, &TestDaemon::uploadConfig);

    ui->configFile->setText(m_testConfig);
    ui->remoteUrl->setText(qgetenv("PLASMASYNC_KIO_REMOTE").constData());

    startDaemon();
}

TestDaemon::~TestDaemon()
{
    delete ui;
}

void TestDaemon::startDaemon()
{
    m_daemon = Daemon::instance();
    Daemon::instance()->addWatch(m_testConfig);

}

void TestDaemon::upload()
{
    // The percent encoding is internal to the KIO backend, so this test will fail
    // for other backends.
    QString file = m_testData + "/local/localrc";
    QString remotefile = m_testData + "remote/" + QUrl::toPercentEncoding(file);
    QFile::remove(remotefile);

    ui->upload_remote->setText(sanitize(remotefile));
    ui->upload_local->setText(sanitize(file));

    m_daemon->upload(file);

    QSignalSpy spy(m_daemon, SIGNAL(uploaded(const QString&)));
    bool uploadsucceeded = spy.wait(100);

    QString u;
    QString h;
    if (uploadsucceeded) {
        u = "uploaded";
    } else {
        u = "upload timed out";
    }

    QByteArray sourcehash = md5sum(file);
    QByteArray remotehash = md5sum(remotefile);
    if (sourcehash == remotehash) {
        h = "hash ok";
    } else {
        h = "checksums differ";
    }
    ui->upload_status->setText(u + " / " + h);
    ui->upload_status->setStyleSheet("background: 'green'");

    QFile::remove(remotefile);
}

QString TestDaemon::sanitize(const QString& t) const
{
    QString out = t;
    //
    auto l = out.split('autotests');
    if (l.count() > 1) {
        out = l[1];
    } else {
        out.replace(m_testData, "DATA");
    }

    return out;
}


void TestDaemon::update()
{
    // The percent encoding is internal to the KIO backend, so this test will fail
    // for other backends.
    QString file = m_testData + "/local/remoterc";
    QString remotefile = m_testData + "remote/" + QUrl::toPercentEncoding(file);
    QFile::copy(m_testData + "/remote/remoterc", remotefile);
    QFile::remove(file);

    ui->update_remote->setText(sanitize(remotefile));
    ui->update_local->setText(sanitize(file));

    m_daemon->update(file);
    QSignalSpy spy(m_daemon, SIGNAL(updated(const QString&)));
    bool downloadsucceeded = spy.wait(100);

    bool success = true;
    QString u;
    QString h;

    if (downloadsucceeded) {
        u = "uploaded";
    } else {
        u = "upload timed out";
        success = false;
    }
    QByteArray sourcehash = md5sum(file);
    QByteArray remotehash = md5sum(remotefile);
    if (sourcehash == remotehash) {
        h = "hash ok";
    } else {
        h = "checksums differ";
        success = false;
    }
    ui->update_status->setText(u + " / " + h);
    ui->update_status->setStyleSheet("background: " + (success ? QString("green") : QString("red")));

    QFile::remove(file);
    QFile::remove(remotefile);
}

void TestDaemon::cleanup()
{
    ui->update_status->setStyleSheet("background: 'transparent'");
    ui->upload_status->setStyleSheet("background: 'transparent'");

}

void TestDaemon::runall()
{
    update();
    upload();
}


void TestDaemon::updateConfig()
{
    m_daemon->update(m_testConfig);

}

void TestDaemon::uploadConfig()
{
    m_daemon->upload(m_testConfig);

}

