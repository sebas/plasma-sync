/*************************************************************************************
 *  Copyright 2014 Sebastian Kügler <sebas@kde.org>                                  *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#include "kiobackend.h"
#include <kio/filecopyjob.h>

#include <KDirWatch>

using namespace PlasmaSync;

Q_LOGGING_CATEGORY(PLASMASYNC_KIO, "plasmasync.kio");

KioBackend::KioBackend(QObject *parent)
    : AbstractBackend(parent)
{
    QLoggingCategory::setFilterRules(QLatin1Literal("plasmasync.kio.debug = true"));
    qCDebug(PLASMASYNC_KIO) << "new KioBackend!";
    const QString remote = qgetenv("PLASMASYNC_KIO_REMOTE").constData();

    if (remote.startsWith('/')) {
        m_remote = QUrl::fromLocalFile(remote);
    } else {
        m_remote = QUrl::fromUserInput(remote);
    }
}

KioBackend::~KioBackend()
{
}

QString KioBackend::name() const
{
    return QString("Kio");
}

bool KioBackend::isValid() const
{
    return true;
}

void KioBackend::upload(const QString& filename)
{
    QUrl src = QUrl::fromLocalFile(filename);
    QUrl dest = m_remote;
    dest.setPath(m_remote.path() + QUrl::toPercentEncoding(filename));
    qCDebug(PLASMASYNC_KIO) << "KioBackend Sending" << dest;

    KIO::FileCopyJob *job = KIO::file_copy(src, dest, 0700, KIO::Overwrite);

    connect(job, &KJob::finished, this, &KioBackend::uploadFinished, Qt::UniqueConnection);
}

void KioBackend::uploadFinished(KJob* job)
{
    KIO::FileCopyJob *copyjob = qobject_cast< KIO::FileCopyJob* >(job);
    QString filename = copyjob->srcUrl().toLocalFile();
//     qCDebug(PLASMASYNC_KIO) << "Upload Finished: " << copyjob->srcUrl();
//     qCDebug(PLASMASYNC_KIO) << "      ------> " << copyjob->destUrl();
    emit uploaded(filename, id());
}

void KioBackend::update(const QString& filename)
{
    QUrl local = QUrl::fromLocalFile(filename);
    QUrl remote = m_remote;
    remote.setPath(m_remote.path() + QUrl::toPercentEncoding(filename));

    qCDebug(PLASMASYNC_KIO) << "KioBackend receiving" << remote;

    KIO::FileCopyJob *job = KIO::file_copy(remote, local, 0755, KIO::Overwrite);

    connect(job, &KJob::finished, this, &KioBackend::updateFinished, Qt::UniqueConnection);
}

void KioBackend::updateFinished(KJob* job)
{
    KIO::FileCopyJob *copyjob = qobject_cast< KIO::FileCopyJob* >(job);
    QString filename = copyjob->destUrl().toLocalFile();
//     qCDebug(PLASMASYNC_KIO) << "Update Finished: " << copyjob->srcUrl();
//     qCDebug(PLASMASYNC_KIO) << "      ------> " << copyjob->destUrl();
    emit updated(filename, id());
}


#include "kiobackend.moc"