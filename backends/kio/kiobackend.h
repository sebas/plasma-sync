/*************************************************************************************
 *  Copyright 2014 Sebastian Kügler <sebas@kde.org>                                  *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#ifndef KIO_BACKEND_H
#define KIO_BACKEND_H

#include "abstractbackend.h"

#include <QLoggingCategory>
#include <QUrl>

class KJob;

namespace PlasmaSync
{

class KioBackend : public PlasmaSync::AbstractBackend
{
    Q_OBJECT
    Q_INTERFACES(PlasmaSync::AbstractBackend)
    Q_PLUGIN_METADATA(IID "org.kde.plasma.plasmasync.kio")

public:
    explicit KioBackend(QObject *parent = 0);
    virtual ~KioBackend();

    virtual QString name() const;
    virtual bool isValid() const;

    virtual void update(const QString &filename);
    virtual void upload(const QString &filename);

private Q_SLOTS:
    void uploadFinished(KJob *job);
    void updateFinished(KJob *job);

private:
    QUrl m_remote;
};
} // namespace

Q_DECLARE_LOGGING_CATEGORY(PLASMASYNC_KIO)

#endif //KIO_BACKEND_H
