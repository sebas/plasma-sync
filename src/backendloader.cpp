/*************************************************************************************
 *  Copyright 2012 by Alejandro Fiestas Olivares <afiestas@kde.org>                  *
 *  Copyright 2014 by Sebastian Kügler <sebas@kde.org>                               *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#include "backendloader.h"
#include "debug_p.h"
#include "abstractbackend.h"

#include <QtCore/QStringList>
#include <QtCore/QCoreApplication>
#include <QtCore/QPluginLoader>
#include <QDir>

using namespace PlasmaSync;

QHash<QString, AbstractBackend*> BackendLoader::s_backends;

int BackendLoader::s_backendCounter = 0;

AbstractBackend* BackendLoader::backend(const QString &id)
{

    //qCDebug(PLASMASYNC) << "Backends: " << s_backends << id;
    QString _id = id;
    if (id.isEmpty()) {
        if (s_backends.count()) {
            return s_backends.value(s_backends.keys().first());
        }
    } else {
        if (s_backends.keys().contains(id)) {
            qCDebug(PLASMASYNC) << "backend aleady loaded.";
            return s_backends.value(id);
        }
    }
    AbstractBackend* backendInstance = loadBackend(id);
    if (_id.isEmpty()) {
        _id = backendInstance->name() + QLatin1Char('-') + QString::number(s_backends.count());
    }

    s_backends.insert(_id, backendInstance);
    qCDebug(PLASMASYNC) << "backend inserted " << _id;

    return backendInstance;
}

QStringList BackendLoader::loadedBackends()
{
    return s_backends.keys();
}


AbstractBackend* BackendLoader::loadBackend(const QString& id)
{
    const QString backend = qgetenv("PLASMASYNC_BACKEND").constData();
    const QString backendFilter = QString::fromLatin1("PlasmaSync_%1*").arg(backend);
    AbstractBackend *backendInstance = 0;

    const QStringList paths = QCoreApplication::libraryPaths();
    Q_FOREACH (const QString &path, paths) {
        const QDir dir(path + QDir::separator() + QLatin1String("/kf5/plasmasync/"),
                       backendFilter,
                       QDir::SortFlags(QDir::QDir::NoSort),
                       QDir::NoDotAndDotDot | QDir::Files);
        const QFileInfoList finfos = dir.entryInfoList();
        Q_FOREACH (const QFileInfo &finfo, finfos) {
            //qCDebug(PLASMASYNC) << "plugin: " << finfo.absoluteFilePath();
            // Skip "Fake" backend unless explicitly specified via PLASMASYNC_BACKEND
            if (backend.isEmpty() && finfo.fileName().contains(QLatin1String("PlasmaSync_Fake"))) {
                continue;
            }


            QPluginLoader loader(finfo.filePath());
            loader.load();
            QObject *instance = loader.instance();
            if (!instance) {
                loader.unload();
                continue;
            }

            backendInstance = qobject_cast< AbstractBackend* >(instance);
            if (backendInstance) {
                if (!backendInstance->isValid()) {
                    qCDebug(PLASMASYNC) << "Skipping" << backendInstance->name() << "backend";
                    delete backendInstance;
                    backendInstance = 0;
                    loader.unload();
                    continue;
                }
                qCDebug(PLASMASYNC) << "Loading" << backendInstance->name() << "backend";
                return backendInstance;
            }
        }
    }

    qCDebug(PLASMASYNC) << "No backend found!";
    return 0;
}
