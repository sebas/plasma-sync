/*************************************************************************************
 *  Copyright 2012 by Alejandro Fiestas Olivares <afiestas@kde.org>                  *
 *  Copyright 2014 by Sebastian Kügler <sebas@kde.org>                               *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#ifndef BACKEND_LOADER_H
#define BACKEND_LOADER_H

#include "abstractbackend.h"
#include <QHash>
#include <QString>

//class AbstractBackend;

class BackendLoader
{
    public:
        static bool init();
        /** Load the backend of a given id
         *
         * @return Pointer to the Backend instance, default backend if id is not provided
         */
        static PlasmaSync::AbstractBackend* backend(const QString &id = QString());

        static QStringList loadedBackends();

    private:
        static QHash<QString, PlasmaSync::AbstractBackend*> s_backends;
        static PlasmaSync::AbstractBackend* loadBackend(const QString &id = QString());
        static int s_backendCounter;
};

#endif //BACKEND_LOADER_H
