/*************************************************************************************
 *  Copyright (C) 2012 by Alejandro Fiestas Olivares <afiestas@kde.org>              *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#include "config.h"

#include "debug_p.h"

#include <KDirWatch>
#include <KSharedConfig>
#include <KConfigGroup>


namespace PlasmaSync {

class Config::Private
{
  public:
    Private():
      valid(true)
    { }

    Private(const Private &other):
      valid(other.valid)
    {
    }

    bool valid;

    QString plugin;
};

Config::Config(QObject *parent)
    : QObject(parent)
    , d(new Private)
{

    KSharedConfig::Ptr config = KSharedConfig::openConfig("plasmasync-testclientrc");
    KConfigGroup cfg(config, "Settings");
    cfg.config()->reparseConfiguration();

//     comboIndex = cfg.readEntry("comboIndex", 1);
//     text = cfg.readEntry("text", "I hung my head.");
//     status = cfg.readEntry("status", "Read from config.");
//     checkbox = cfg.readEntry("checkbox", true);

}

bool Config::isValid() const
{
    return d->valid;
}

void Config::setValid(bool valid)
{
    d->valid = valid;
}

} //PlasmaSync namespace
