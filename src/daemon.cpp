/*************************************************************************************
 *  Copyright (C) 2012 by Alejandro Fiestas Olivares <afiestas@kde.org>              *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#include "daemon.h"
#include "daemonadaptor.h"

#include "abstractbackend.h"
#include "debug_p.h"
#include "backendloader.h"

#include <KDirWatch>

namespace PlasmaSync {

Daemon* Daemon::s_daemon = 0;

class Daemon::Private
{
  public:
    Private():
      valid(true),
      blockUploads(false)
    { }

    Private(const Private &other):
      valid(other.valid)
    {
    }

    bool valid;

    QStringList blockUpdates;
    bool blockUploads;
    QHash<QString, QString> watchedFiles;
};

Daemon::Daemon(QObject *parent)
    : QObject(parent)
    , d(new Private)
{
    start();
    // Catch both, direct changes to the config file ...
    connect(KDirWatch::self(), &KDirWatch::dirty, this, &Daemon::watchedFileChanged);
    // ... but also remove/recreate cycles, like KConfig does it
    connect(KDirWatch::self(), &KDirWatch::created, this, &Daemon::watchedFileChanged);
}

Daemon* Daemon::instance()
{
    if (!s_daemon) {
        s_daemon = new Daemon();
    }
    return s_daemon;
}

void Daemon::start()
{
    new DaemonAdaptor(this);

    QDBusConnection connection = QDBusConnection::sessionBus();
    bool ret = connection.registerService("org.kde.plasma.sync.Daemon");
    ret = connection.registerObject("/", this);
}


bool Daemon::isValid() const
{
    return d->valid;
}

void Daemon::setValid(bool valid)
{
    d->valid = valid;
}

void Daemon::addWatch(const QString& filename, const QString& backendId)
{
    // Disable
    d->blockUpdates << filename;
    update(filename, backendId);
    qCDebug(PLASMASYNC_KIO) << "Watching file: " << filename;
    d->watchedFiles[filename] = backendId;
    KDirWatch::self()->addFile(filename);
}

void Daemon::removeWatch(const QString& filename)
{
    KDirWatch::self()->removeFile(filename);
    d->watchedFiles.remove(filename);
}

void Daemon::watchedFileChanged(const QString& filename)
{
    if (d->blockUpdates.contains(filename)) {
        d->blockUpdates.removeAll(filename);
        return;
    }
    qCDebug(PLASMASYNC) << "File changed: " << filename << d->watchedFiles;
    if (d->watchedFiles.contains(filename)) {
        auto backend = BackendLoader::backend(d->watchedFiles[filename]);
        backend->upload(filename);
    }
}

void Daemon::update(const QString& filename, const QString& backendId)
{
    auto backend = BackendLoader::backend(backendId);
    connect(backend, &AbstractBackend::updated, this, &Daemon::announceUpdated, Qt::UniqueConnection);
    backend->update(filename);
}

void Daemon::announceUpdated(const QString& filename, const QString& backendId)
{
    qCDebug(PLASMASYNC) << " Updated!:" << filename;
    emit updated(filename);
}

void Daemon::upload(const QString& filename, const QString& backendId)
{
    auto backend = BackendLoader::backend(backendId);
    connect(backend, &AbstractBackend::uploaded, this, &Daemon::announceUploaded, Qt::UniqueConnection);
    backend->upload(filename);
}

void Daemon::announceUploaded(const QString& filename, const QString& backendId)
{
    qCDebug(PLASMASYNC) << " Uploaded!:" << filename;
    emit uploaded(filename);
}


} //PlasmaSync namespace

#include "daemon.moc"
