/*************************************************************************************
 *  Copyright 2012 by Alejandro Fiestas Olivares <afiestas@kde.org>                  *
 *  Copyright 2014 by Sebastian Kügler <sebas@kde.org>                               *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#ifndef PLASMASYNC_DAEMON_H
#define PLASMASYNC_DAEMON_H

#include "plasmasync_export.h"

#include <QtCore/QHash>
#include <QtCore/QObject>
#include <QtCore/QMetaType>

namespace PlasmaSync {

/**
 * Represents a synchronization daemon. This is the out-of-process part that 
 * takes care of low-level synching through backends.
 *
 * This is the main class of PlasmaSync, with it you can use
 * the static methods instance() to get the daemon instance, or create one.
 *
 */
class PLASMASYNC_EXPORT Daemon : public QObject
{
    Q_OBJECT

public:
    /**
     * Gets the daemon's singleton. Use this function to get the instance of the Daemon.
     *
     * @return Pointer to the Daemon singleton, or null on error.
     */
    static Daemon* instance();

    /** Adds a file to the watched files.
     *
     * This means the local file is watched for changes, whenever the file changes,
     * an upload is scheduled.
     *
     * Optionally, you can specify a backend which should be used for remote storage.
     * This allows you to use different backends for certain settings. Note that you
     * can not add a file to more than one backend.
     *
     * @param filename The full path to the file to watch
     * @param backendId The id of the backend to use, leave out for the default or
     * current backend of the file.
     */
    void addWatch(const QString &filename, const QString &backendId = QString());

    /** Remove a file from the watched files.
     *
     * This means that the local file is not watched for changes anymore.
     *
     * Optionally, you can specify a backend which should be used for remote storage.
     * This allows you to use different backends for certain settings. Note that you
     * can not add a file to more than one backend.
     *
     * @param filename The full path to the file to remove from the list of watched
     * files.
     */
    void removeWatch(const QString &filename);

    /** Schedule an update for file to be downloaded through the backend.
     *
     * Call this function to request an updated file from the server. This call may
     * not return quickly, depending on network conditions. It just asks the daemon
     * check for updates once network and other conditions allow it.
     *
     * Optionally, you can specify a backend which should be used for remote storage.
     * This allows you to use different backends for certain settings. Note that you
     * can not add a file to more than one backend.
     *
     * @param filename The full path to the file to update
     * @param backendId The id of the backend to use, leave out for the default or
     * current backend of the file.
     */
    void update(const QString &filename, const QString &backendId = QString());

    /** Schedule an upload for a file.
     *
     * Call this function to tell the daemon that this is a good time to upload a file.
     * This call will not finish with the uploaded signal immediately, it rather asks
     * the backend to sync this file up to the server.
     *
     * Optionally, you can specify a backend which should be used for remote storage.
     * This allows you to use different backends for certain settings. Note that you
     * can not add a file to more than one backend.
     *
     * @param filename The full path to the file to update
     * @param backendId The id of the backend to use, leave out for the default or
     * current backend of the file.
     */
    void upload(const QString &filename, const QString &backendId = QString());

    void setValid(bool valid);
    bool isValid() const;

Q_SIGNALS:
    void updated(const QString &file);
    void uploaded(const QString &file);

private:
    /** Private ctor to prevent this object from being created directly.
     *
     * Users should use Daemon::instance() instead.
     */
    explicit Daemon(QObject *parent = 0);
    Q_DISABLE_COPY(Daemon)
    static Daemon* s_daemon;

    class Private;
    Private * const d;

    Daemon(Private *dd);

    void announceUpdated(const QString& filename, const QString& backendId);
    void announceUploaded(const QString& filename, const QString& backendId);
    void watchedFileChanged(const QString& filename);

    void start();
};

} //PlasmaSync namespace


#endif //PLASMASYNC_DAEMON_H
