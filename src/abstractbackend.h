/*************************************************************************************
 *  Copyright 2014 by Sebastian Kügler <sebas@kde.org>                               *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#ifndef PLASMASYNC_ABSTRACT_BACKEND_H
#define PLASMASYNC_ABSTRACT_BACKEND_H

#include <QString>
#include <QObject>

#include "plasmasync_export.h"

/** Abstract class for backends.
 */
namespace PlasmaSync {

class PLASMASYNC_EXPORT AbstractBackend : public QObject
{
    Q_OBJECT

    public:
        explicit AbstractBackend(QObject *parent = 0);
        virtual ~AbstractBackend() {}
        virtual QString name() const = 0;
        virtual bool isValid() const = 0;
        virtual void update(const QString &filename) = 0;
        virtual void upload(const QString &filename) = 0;

        QString id() const;
        void setId(const QString &_id);

    Q_SIGNALS:
        void updated(const QString &filename, const QString &backend);
        void uploaded(const QString &filename, const QString &backend);
        void error(const QString &filename, const QString &backend, const QString &errorText);

    protected:
        QString m_id;
        class Private;
        Private * const d;

};

} // namespace

Q_DECLARE_INTERFACE(PlasmaSync::AbstractBackend, "org.kde.plasma.plasmasync")

#endif //PLASMASYNC_ABSTRACT_BACKEND_H
